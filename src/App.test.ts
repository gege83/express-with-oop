import request from "supertest";
import App from "./App";
import {Router} from "express";

describe("GET /api", () => {
    it("should return 200 OK", () => {
        const testRouterFactory = {createRoutes: () => Router().get("/", (req, res) => res.json({OK: true}))};
        return request(new App({"/api": [testRouterFactory]}).start(3000)).get("/api")
            .expect(200);
    });
});
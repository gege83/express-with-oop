import express from "express";
import {RouterFactory} from "./routes/RouterFactory";

interface Mapping {
    [url: string]: RouterFactory[];
}

class App {
    private mapping: Mapping;

    public constructor(mapping: Mapping) {
        this.mapping = mapping;
    }

    public start(port: number) {
        const app = express();
        Object.keys(this.mapping).forEach((url) => {
            this.mapping[url].forEach((factory) => app.use(url, factory.createRoutes()));
        });
        app.listen(port, () => {
            console.log(`app started on ${port}`);
        });
        return app;
    }
}

export default App;
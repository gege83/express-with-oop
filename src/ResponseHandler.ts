import {ResponseHandler} from "./routes/HealthRouter";
import {Response} from "express";

export default class JsonExpressResponseHandler implements ResponseHandler {
    public sendSimpleResponse(res: Response, message: any = {}) {
        res.json(message);
    };
}
import HealthController from "./HealthController";
import {ResponseHandler} from "../routes/HealthRouter";

describe("HealthController", () => {
    it("health handles request and send {ok:true} back", () => {
        const responseHandler: ResponseHandler = {sendSimpleResponse: jest.fn()};
        let response = {};
        new HealthController(responseHandler).health({} as any, response as any);
        expect(responseHandler.sendSimpleResponse).toHaveBeenLastCalledWith(response, {ok: true});
    });
});
import {ResponseHandler} from "../routes/HealthRouter";
import {Response, Request} from "express";

export default class HealthController {
    private responseHandler: ResponseHandler;

    public constructor(responseHandler: ResponseHandler) {
        this.responseHandler = responseHandler;
        this.health = this.health.bind(this);
    }

    public health(req: Request, res: Response) {
        this.responseHandler.sendSimpleResponse(res, {ok: true});
    };
}
import App from "./App";
import HealthRouter from "./routes/HealthRouter";
import JsonExpressResponseHandler from "./ResponseHandler";

const responseHandler = new JsonExpressResponseHandler();
const healthRouter = new HealthRouter(responseHandler);
const mapping = {"/api/v1": [healthRouter]};

new App(mapping).start(3000);
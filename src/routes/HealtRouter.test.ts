jest.mock("../controller/HealthController");
import HealthRouter from "./HealthRouter";
import HealthController from "../controller/HealthController";
import JsonExpressResponseHandler from "../ResponseHandler";

describe("HealthRouter", () => {
    it("creates health route", () => {
        const router = new HealthRouter(new JsonExpressResponseHandler()).createRoutes();
        const route = router.stack.find(s => s.route.path === '/health');
        expect(route).toBeDefined();
        const stack = route.route.stack.find((s: any) => s.method === 'get');//.find((s:any) => s.path === '/health');
        const healthControllerInstance = (HealthController as jest.Mock).mock.instances[0];
        expect(stack.handle).toEqual(healthControllerInstance.health);
    });
});
import HealthController from "../controller/HealthController";
import {Router} from "express";
import {RouterFactory} from "./RouterFactory";
import {Response} from "express";

export interface ResponseHandler {
    sendSimpleResponse: (res: Response, message: any) => void;
}

export default class HealthRouter implements RouterFactory {
    private responseHandler: ResponseHandler;

    public constructor(responseHandler: ResponseHandler) {
        this.responseHandler = responseHandler;
    }

    public createRoutes(): Router {
        const healthController = new HealthController(this.responseHandler);
        return Router().get("/health", healthController.health);// .bind(healthController));
    }
}

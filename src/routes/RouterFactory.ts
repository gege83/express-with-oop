import {Router} from "express";

export interface RouterFactory {
    createRoutes: () => Router;
}